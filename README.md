# Self-Point Theme Css Builder
## How to use
`window.spBuildCss` is the function to build, You must sen the following parameters:

| Name        | Type             | Required |
|-------------|------------------|----------|
| cssStr      | String           | true     |
| themeConfig | Object           | true     |
| element     | HTMLStyleElement | true     |

<!--
## description
save your css as `color: {{mainColor}};`
You need to have themeConfig, for example `{mainColor: '#207d3f'}`
Add in your html <style id="theme_css" type="text/css"></style>
run `window.spBuildCss(cssStr, themeConfig, document.getElementById('theme_css'))`
and it will add the converted css to your html-->

## Contribute
After you change the file `sp-theme-css.js` you need to run `gulp dist` before you commit.
