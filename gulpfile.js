var gulp = require('gulp'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    git = require('gulp-git');

gulp.task('default', ['dist']);

gulp.task('dist', function () {
    return gulp.src('sp-theme-css.js')
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-theme-css.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
});